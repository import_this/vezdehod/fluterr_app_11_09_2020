import 'package:flutter/material.dart';
import 'globals.dart' as globals;

class FoundryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        /*
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios, color: Color(0xFF3F8AE0)),
            onPressed: () => Navigator.of(context).pop(),
          ),
          backgroundColor: Color(0xFFFFFFFF),
          elevation: 0,
          title: Text(
            "Регулярный сбор",
            style: TextStyle(color: Color(0xFF000000)),
          ),
        ),
        */
        body: SingleChildScrollView(
            child: Column(
      children: [
        SizedBox(
            width: double.infinity,
            height: 140,
            child: globals.dataStore["image"] == null
                ? null
                : FittedBox(
                    child: Image.file(globals.dataStore["image"]),
                    fit: BoxFit.cover)),
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                globals.dataStore["foundryName"],
                style: TextStyle(
                  fontSize: 24,
                ),
              )
            ],
          ),
        ),
      ],
    )));
  }
}
