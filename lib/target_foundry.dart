import 'package:flutter/material.dart';
import 'package:test_project/vk_widgets/full_button.dart';
import 'package:test_project/vk_widgets/image_picker.dart';
import 'package:test_project/vk_widgets/input.dart';
import 'package:test_project/vk_widgets/select.dart';

import 'foundry_page.dart';

class TargetFoundry extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios, color: Color(0xFF3F8AE0)),
            onPressed: () => Navigator.of(context).pop(),
          ),
          backgroundColor: Color(0xFFFFFFFF),
          elevation: 0,
          title: Text(
            "Целевой сбор",
            style: TextStyle(color: Color(0xFF000000)),
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              VkImagePicker(),
              VkInputText(
                label: "Название сбора",
                hint: "Название сбора",
                storeKey: "foundryName",
              ),
              VkInputText(
                label: "Сумма, ₽",
                hint: "Сколько нужно собрать",
                type: TextInputType.number,
                storeKey: "foundryValue",
              ),
              VkInputText(
                label: "Цель",
                hint: "Например, лечение человека",
                storeKey: "foundryGoal",
              ),
              VkInputText(
                label: "Описание",
                hint: "На что пойдут деньги и как они комуто помогут?",
                storeKey: "foundryDesc",
              ),
              VkSelect(label: "Куда получать деньги", values: [
                "Счет VK pay • 1234",
                "Счет VK pay • 6912",
              ]),
              VkFullButton(onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => FoundryPage()),
                );
              })
            ]),
          ),
        ));
  }
}
