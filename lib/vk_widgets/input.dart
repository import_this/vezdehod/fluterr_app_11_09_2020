import 'package:flutter/material.dart';
import 'package:test_project/vk_widgets/text.dart';
import '../globals.dart' as globals;

class VkInputText extends StatefulWidget {
  final String label;
  final String hint;
  final TextInputType type;
  final String storeKey;

  VkInputText({
    Key key,
    this.label,
    this.hint,
    this.type = TextInputType.text,
    this.storeKey = "test",
  }) : super(key: key);

  @override
  _VkInputText createState() => _VkInputText();
}

// Define a corresponding State class.
// This class holds the data related to the Form.

class _VkInputText extends State<VkInputText> {
  final myController = TextEditingController();

  @override
  void initState() {
    super.initState();
    myController.text = globals.dataStore[widget.storeKey];
    myController.addListener(storeValue);
  }

  storeValue() {
    globals.dataStore[widget.storeKey] = myController.text;
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 12.0, bottom: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: VkText(
              widget.label,
              fontSize: 14,
            ),
          ),
          TextField(
            keyboardType: widget.type,
            textCapitalization: TextCapitalization.words,
            maxLines: 100,
            minLines: 1,
            controller: myController,
            decoration: InputDecoration(
              filled: true,
              fillColor: Color(0xFFF2F3F5),
              contentPadding: EdgeInsets.all(12),
              isDense: true,
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(
                    color: Color.fromARGB(30, 0, 0, 0), //Color of the border
                    style: BorderStyle.solid, //Style of the border
                    width: 0.5, //width of the border
                  )),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(
                    color: Color.fromARGB(30, 0, 0, 0), //Color of the border
                    style: BorderStyle.solid, //Style of the border
                    width: 0.5, //width of the border
                  )),
              hintText: widget.hint,
              hintStyle: TextStyle(fontSize: 16.0, color: Color(0xFF818C99)),
            ),
          ),
        ],
      ),
    );
  }
}
