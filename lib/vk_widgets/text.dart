import 'dart:ffi';

import 'package:flutter/material.dart';

class VkText extends StatelessWidget {
  final String text;
  final double fontSize;

  const VkText(
    this.text, {
    Key key,
    this.fontSize = 16,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(this.text,
        style: TextStyle(color: Color(0xFF818C99), fontSize: this.fontSize));
  }
}
