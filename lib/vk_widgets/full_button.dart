import 'package:flutter/material.dart';

class VkFullButton extends StatelessWidget {
  final Function onPressed;

  const VkFullButton({
    Key key,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 12, bottom: 12),
      child: SizedBox(
        width: double.infinity,
        height: 44,
        child: FlatButton(
          color: Color(0xFF4986CC),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0)),
          textColor: Color(0xFFFFFFFF),
          child: new Text('Далее'),
          onPressed: () {
            this.onPressed();
          },
        ),
      ),
    );
  }
}
