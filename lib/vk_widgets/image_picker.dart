import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import '../globals.dart' as globals;

class VkImagePicker extends StatefulWidget {
  const VkImagePicker({
    Key key,
  }) : super(key: key);

  @override
  _VkImagePicker createState() => _VkImagePicker();
}

class _VkImagePicker extends State<VkImagePicker> {
  File image;

  selectImage() async {
    image = await ImagePicker.pickImage(source: ImageSource.gallery);
    globals.dataStore["image"] = image;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 140,
      child: image == null
          ? OutlineButton(
              borderSide: BorderSide(
                color: Color(0xFF3F8AE0), //Color of the border
                style: BorderStyle.solid, //Style of the border
                width: 1, //width of the border
              ),
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(10.0)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.image,
                    color: Color(0xFF3F8AE0),
                  ),
                  Text(
                    "Загрузить обложку",
                    style: TextStyle(
                        color: Color(0xFF3F8AE0),
                        fontSize: 16,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              ),
              onPressed: () {
                this.selectImage();
              },
            )
          : ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: FittedBox(child: Image.file(image), fit: BoxFit.cover)),
    );
  }
}
