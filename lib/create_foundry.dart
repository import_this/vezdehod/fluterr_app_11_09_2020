import 'package:flutter/material.dart';
import 'package:test_project/regular_foundry.dart';
import 'package:test_project/target_foundry.dart';
import 'package:test_project/vk_widgets/text.dart';

class CreateFoundry extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Color(0xFF3F8AE0)),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0,
        title: Text(
          "Тип сбора",
          style: TextStyle(color: Color(0xFF000000)),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            VkOutlineButton(
              mainIcon: Icons.my_location,
              firstText: "Целевой сбор",
              secondText: "Когда есть определённая цель",
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => TargetFoundry()),
                );
              },
            ),
            VkOutlineButton(
                mainIcon: Icons.calendar_today,
                firstText: "Регулярный сбор",
                secondText: "Если помощь нужна ежемесячно",
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => RegularFoundry()),
                  );
                }),
          ],
        ),
      ),
    );
  }
}

class VkOutlineButton extends StatelessWidget {
  final IconData mainIcon;
  final String firstText;
  final String secondText;
  final VoidCallback onPressed;

  const VkOutlineButton({
    Key key,
    this.mainIcon,
    this.firstText,
    this.secondText,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 12),
      child: OutlineButton(
        padding: EdgeInsets.all(12),
        onPressed: () {
          this.onPressed();
        },
        color: Color(0xFFF5F5F5),
        borderSide: BorderSide(
          color: Color.fromARGB(20, 0, 0, 0), //Color of the border
          style: BorderStyle.solid, //Style of the border
          width: 0.33, //width of the border
        ),
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(10.0)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Icon(this.mainIcon, color: Color(0xFF3F8AE0)),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(this.firstText),
                        VkText(
                          this.secondText,
                          fontSize: 13,
                        )
                      ]),
                )
              ],
            ),
            Icon(
              Icons.arrow_forward_ios,
              color: Color(0xFFB8C1CC),
            )
          ],
        ),
      ),
    );
  }
}
