import 'package:flutter/material.dart';
import 'package:test_project/vk_widgets/text.dart';

class VkSelect extends StatelessWidget {
  final String label;

  final List<String> values;

  VkSelect({
    Key key,
    this.label,
    this.values,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 12.0, bottom: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: VkText(
              this.label,
              fontSize: 14,
            ),
          ),
          SizedBox(
            width: double.infinity,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color(0xFFF2F3F5),
                border: Border.all(
                    color: Color.fromARGB(30, 0, 0, 0),
                    style: BorderStyle.solid,
                    width: 0.50),
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 12.0, right: 12),
                child: DropdownButton(
                    icon: Icon(
                      Icons.keyboard_arrow_down,
                      size: 30,
                      color: Color(0xFFB8C1CC),
                    ),
                    isExpanded: true,
                    underline: Container(
                      height: 0,
                      color: Colors.deepPurpleAccent,
                    ),
                    items: values
                        .map((value) => DropdownMenuItem(
                              child: Text(value),
                              value: value,
                            ))
                        .toList(),
                    value: values.first,
                    onChanged: (String newValue) {}),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
